import {app, BrowserWindow, desktopCapturer, ipcMain} from 'electron';
import * as ffi from 'ffi-napi'
import * as ref from 'ref-napi'

declare const MAIN_WINDOW_WEBPACK_ENTRY: any;

export interface ALTTABINFO_Struct {
    cbSize: number
    cItems: number
    cColumns: number
    cRows: number
    iColFocus: number
    iRowFocus: number
    cxItem: number
    cyItem: number
    ptStart: Buffer
}

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
    app.quit();
}

const createWindow = (): void => {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        height: 800,
        width: 800,
        //transparent: true,
        //frame: false
        webPreferences: {
            nodeIntegration: true
        }
    });

    // remove stupid menu
    mainWindow.setMenu(null);

    // and load the index.html of the app.
    mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

    // Open the DevTools.
    mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

ipcMain.on('getWindows', (event, arg) => {
    const count = BrowserWindow.getAllWindows().length;
    event.sender.send('channelTest', count)
})

ipcMain.on('getOpenWindows', async (event, args) => {
    console.log("Method called")
    let tmpSources: string[] = [];
    await desktopCapturer.getSources({types: ['window', 'screen']}).then(async sources => {
        console.log("Try to fetch shit")
        console.log("found sources: ", sources.length)
        for (const source of sources) {
            tmpSources.push(source.name);
        }
    });
    console.log("sending back: ", tmpSources.length)
    event.sender.send('getOpenWindowsCompleted', tmpSources)
})

function TEXT(text: string) {
    return new Buffer(text, 'ucs2').toString('binary');
}


const voidPtr = ref.refType(ref.types.void);
const stringPtr = ref.refType(ref.types.CString);


const user32 = new ffi.Library('user32', {
    'MessageBoxW': [
        'int32', ['int32', 'string', 'string', 'int32']
    ],
    'EnumWindows': [
        'bool', [voidPtr, 'int32']
    ],
    'GetWindowTextA': ['long', ['long', stringPtr, 'long']],
    'GetParent': ['long', ['long']],
    'GetAltTabInfoW': ['bool', ['long', 'int32', 'int32', 'uint16', "int32"]],
    'GetTopWindow': ['long', ['long']],
    'IsWindowVisible': ['bool', ['long']],
    'GetShellWindow': ['long', []],
    'GetAncestor': ['long', ['long', 'uint']],
    'GetLastActivePopup': ['long', ['long']],
    'GetClassNameA': ['long', ['long', stringPtr, 'long']]
});

const OK_or_Cancel = user32.MessageBoxW(
    0, TEXT('I am Node.JS!'), TEXT('Hello, World!'), 1
);

// Recursive function which returns the visible active popup of a window
function getLastVisibleActivePopUpOfWindow(hwnd: any): any {
    //Get last active popup for rootWindow which Determines which pop-up window owned
    // by the specified window was most recently active
    const lastActiveWindow = user32.GetLastActivePopup(hwnd);
    //console.log(`LAST ACTIVE WINDOW FOR ${hwnd} WAS: ${lastActiveWindow}`)

    if (user32.IsWindowVisible(lastActiveWindow)) {
        return lastActiveWindow;
    } else if (lastActiveWindow == hwnd) {
        return voidPtr;
    } else {
        return getLastVisibleActivePopUpOfWindow(lastActiveWindow);
    }
}


const windowProc = ffi.Callback('bool', ['long', 'int32'], function (hwnd: any, lParam: any) {
    // Exclude Desktop Window
    const desktopWindowHwnd = user32.GetShellWindow()
    if (hwnd === desktopWindowHwnd) {
        //console.log(`FOUND DESKTOP WINDOW ${hwnd}. RETURNING`)
        return false
    }

    // Get parent window of window
    const rootWindow = user32.GetAncestor(hwnd, 3);
    //console.log(`FOUND ROOT WINDOW FOR ${hwnd}: ${rootWindow}`)

    if (getLastVisibleActivePopUpOfWindow(rootWindow) === hwnd) {
        let windowClassNameBuffer = new Buffer(255);
        let amount = user32.GetClassNameA(hwnd, windowClassNameBuffer, 255);
        let windowClassName = ref.readCString(windowClassNameBuffer);

        let buf, name, ret;
        buf = new Buffer(255);
        ret = user32.GetWindowTextA(hwnd, buf, 255);
        name = ref.readCString(buf, 0);
        if (name !== '' && windowClassName !== 'Windows.UI.Core.CoreWindow' && windowClassName !== 'ApplicationFrameWindow') {
            console.log(`Name: ${name}, Classname: ${windowClassName}, AMOUNT: ${amount}`)
        }
    }

    return true;
});

user32.EnumWindows(windowProc, 0);


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
