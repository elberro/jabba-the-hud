import * as React from 'react';
import * as ReactDOM from 'react-dom';
import electron from "electron";


interface MainHUDComponentProps {
}

export default class MainHUDComponent extends React.Component<MainHUDComponentProps> {

    state = {
       openWindowCount: 0,
       sources: [] as string[]
    }

    changeState = (x: number) => {
        this.setState({
            openWindowCount: x
        })
    }

    changeSourcesState = (sources: string[]) => {
        this.setState({
            sources: sources
        })
    }

    constructor(props: MainHUDComponentProps | Readonly<MainHUDComponentProps>) {
        super(props);
        const ipc = electron.ipcRenderer
        ipc.send('getWindows')
        ipc.send('getOpenWindows')
        ipc.on('channelTest', (event, payload) => {
            this.changeState(payload);
        });
        ipc.on('getOpenWindowsCompleted', (event, payload) => {
            this.changeSourcesState(payload);
        });
    }

    //this.props.data.getWindows()

    render() {
        return (
            <div>
                <h1>TEST THAT SHIT {this.state.openWindowCount}</h1>
                {this.state.sources.map(source => {
                    return (
                        <li>{source}</li>
                    )
                })}
                <p>AHAHHAHAH</p>
            </div>
        )
    }
}